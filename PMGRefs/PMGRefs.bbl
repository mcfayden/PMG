% $ biblatex auxiliary file $
% $ biblatex version 2.5 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\entry{Gleisberg:2008ta}{article}{}
  \name{author}{1}{}{%
    {{}%
     {al}{a.}%
     {T.}{T.}%
     {Gleisberg~et}{G.~e.}%
     {}{}}%
  }
  \strng{namehash}{aTGe1}
  \strng{fullhash}{aTGe1}
  \verb{doi}
  \verb 10.1088/1126-6708/2009/02/007
  \endverb
  \verb{eprint}
  \verb 0811.4622
  \endverb
  \field{pages}{007}
  \field{title}{{Event generation with SHERPA 1.1}}
  \field{volume}{0902}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2009}
\endentry

\entry{Gleisberg:2008fv}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Gleisberg}{G.}%
     {Tanju}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {H{\"o}che}{H.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{GTHS1}
  \strng{fullhash}{GTHS1}
  \verb{doi}
  \verb 10.1088/1126-6708/2008/12/039
  \endverb
  \verb{eprint}
  \verb 0808.3674
  \endverb
  \field{pages}{039}
  \field{title}{{Comix, a new matrix element generator}}
  \field{volume}{0812}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Schumann:2007mg}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Schumann}{S.}%
     {Steffen}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Krauss}{K.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{SSKF1}
  \strng{fullhash}{SSKF1}
  \verb{doi}
  \verb 10.1088/1126-6708/2008/03/038
  \endverb
  \verb{eprint}
  \verb 0709.1027
  \endverb
  \field{pages}{038}
  \field{title}{{A Parton shower algorithm based on Catani-Seymour dipole
  factorisation}}
  \field{volume}{0803}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Hoeche:2011fd}{article}{}
  \name{author}{4}{}{%
    {{}%
     {Hoeche}{H.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Krauss}{K.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Schonherr}{S.}%
     {Marek}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Siegert}{S.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HS+2}
  \strng{fullhash}{HSKFSMSF2}
  \verb{doi}
  \verb 10.1007/JHEP09(2012)049
  \endverb
  \verb{eprint}
  \verb 1111.1220
  \endverb
  \field{pages}{049}
  \field{title}{{A critical appraisal of NLO+PS matching methods}}
  \field{volume}{09}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{Hoeche:2012yf}{article}{}
  \name{author}{4}{}{%
    {{}%
     {H{\"o}che}{H.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Krauss}{K.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Sch{\"o}nherr}{S.}%
     {Marek}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Siegert}{S.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HS+1}
  \strng{fullhash}{HSKFSMSF1}
  \verb{doi}
  \verb 10.1007/JHEP04(2013)027
  \endverb
  \verb{eprint}
  \verb 1207.5030
  \endverb
  \field{pages}{027}
  \field{title}{{QCD matrix elements + parton showers: The NLO case}}
  \field{volume}{1304}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2013}
\endentry

\entry{Cascioli:2011va}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Cascioli}{C.}%
     {Fabio}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Maierhofer}{M.}%
     {Philipp}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Pozzorini}{P.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CFMPPS1}
  \strng{fullhash}{CFMPPS1}
  \verb{doi}
  \verb 10.1103/PhysRevLett.108.111601
  \endverb
  \verb{eprint}
  \verb 1111.5206
  \endverb
  \field{pages}{111601}
  \field{title}{{Scattering Amplitudes with Open Loops}}
  \field{volume}{108}
  \field{journaltitle}{Phys. Rev. Lett.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{Denner:2016kdg}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Denner}{D.}%
     {Ansgar}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Dittmaier}{D.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Hofer}{H.}%
     {Lars}{L.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DADSHL1}
  \strng{fullhash}{DADSHL1}
  \verb{doi}
  \verb 10.1016/j.cpc.2016.10.013
  \endverb
  \verb{eprint}
  \verb 1604.06792
  \endverb
  \field{pages}{220\bibrangedash 238}
  \field{title}{{Collier: a fortran-based Complex One-Loop LIbrary in Extended
  Regularizations}}
  \field{volume}{212}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2017}
\endentry

\entry{Alwall:2014hca}{article}{}
  \name{author}{10}{}{%
    {{}%
     {Alwall}{A.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Frederix}{F.}%
     {R.}{R.}%
     {}{}%
     {}{}}%
    {{}%
     {Frixione}{F.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Hirschi}{H.}%
     {V.}{V.}%
     {}{}%
     {}{}}%
    {{}%
     {Maltoni}{M.}%
     {F.}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Mattelaer}{M.}%
     {O.}{O.}%
     {}{}%
     {}{}}%
    {{}%
     {Shao}{S.}%
     {H.~S.}{H.~S.}%
     {}{}%
     {}{}}%
    {{}%
     {Stelzer}{S.}%
     {T.}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Torrielli}{T.}%
     {P.}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Zaro}{Z.}%
     {M.}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{AJ+1}
  \strng{fullhash}{AJFRFSHVMFMOSHSSTTPZM1}
  \verb{doi}
  \verb 10.1007/JHEP07(2014)079
  \endverb
  \verb{eprint}
  \verb 1405.0301
  \endverb
  \field{pages}{079}
  \field{title}{{The automated computation of tree-level and next-to-leading
  order differential cross sections, and their matching to parton shower
  simulations}}
  \field{volume}{1407}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2014}
\endentry

\entry{Sjostrand:2007gs}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Sjostrand}{S.}%
     {T.}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Mrenna}{M.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Skands}{S.}%
     {P.}{P.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{STMSSP1}
  \strng{fullhash}{STMSSP1}
  \verb{doi}
  \verb 10.1016/j.cpc.2008.01.036
  \endverb
  \verb{eprint}
  \verb 0710.3820
  \endverb
  \field{pages}{852\bibrangedash 867}
  \field{title}{{A brief introduction to PYTHIA 8.1}}
  \field{volume}{178}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Lonnblad:2001iq}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Lonnblad}{L.}%
     {Leif}{L.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{LL1}
  \strng{fullhash}{LL1}
  \verb{doi}
  \verb 10.1088/1126-6708/2002/05/046
  \endverb
  \verb{eprint}
  \verb hep-ph/0112284
  \endverb
  \field{pages}{046}
  \field{title}{{Correcting the color dipole cascade model with fixed order
  matrix elements}}
  \field{volume}{05}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2002}
\endentry

\entry{Lonnblad:2011xx}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Lonnblad}{L.}%
     {Leif}{L.}%
     {}{}%
     {}{}}%
    {{}%
     {Prestel}{P.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{LLPS1}
  \strng{fullhash}{LLPS1}
  \verb{doi}
  \verb 10.1007/JHEP03(2012)019
  \endverb
  \verb{eprint}
  \verb 1109.4829
  \endverb
  \field{pages}{019}
  \field{title}{{Matching Tree-Level Matrix Elements with Interleaved Showers}}
  \field{volume}{03}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{ATL-PHYS-PUB-2014-021}{booklet}{}
  \name{author}{1}{}{%
    {{}%
     {{ATLAS Collaboration}}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \field{howpublished}{{ATL-PHYS-PUB-2014-021}}
  \field{title}{{ATLAS Pythia~8 tunes to $7\;\mbox{TeV}$ data}}
  \verb{url}
  \verb http://cdsweb.cern.ch/record/1966419
  \endverb
  \field{year}{2014}
\endentry

\entry{Ball:2012cx}{article}{}
  \true{moreauthor}
  \name{author}{5}{}{%
    {{}%
     {Ball}{B.}%
     {Richard~D.}{R.~D.}%
     {}{}%
     {}{}}%
    {{}%
     {Bertone}{B.}%
     {Valerio}{V.}%
     {}{}%
     {}{}}%
    {{}%
     {Carrazza}{C.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Deans}{D.}%
     {Christopher~S.}{C.~S.}%
     {}{}%
     {}{}}%
    {{}%
     {Del~Debbio}{D.~D.}%
     {Luigi}{L.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BRD+1}
  \strng{fullhash}{BRDBVCSDCSDDL+1}
  \verb{doi}
  \verb 10.1016/j.nuclphysb.2012.10.003
  \endverb
  \verb{eprint}
  \verb 1207.1303
  \endverb
  \field{pages}{244\bibrangedash 289}
  \field{title}{{Parton distributions with LHC data}}
  \field{volume}{867}
  \field{journaltitle}{Nucl. Phys. B}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2013}
\endentry

\entry{EvtGen}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{D. J. Lange}}{D.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{D1}
  \strng{fullhash}{D1}
  \verb{doi}
  \verb 10.1016/S0168-9002(01)00089-4
  \endverb
  \field{pages}{152}
  \field{title}{{The EvtGen particle decay simulation package}}
  \field{volume}{A462}
  \field{journaltitle}{Nucl. Instrum. Meth.}
  \field{eprintclass}{hep-ex}
  \field{year}{2001}
\endentry

\entry{Frederix:2012ps}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Frederix}{F.}%
     {Rikkert}{R.}%
     {}{}%
     {}{}}%
    {{}%
     {Frixione}{F.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{FRFS1}
  \strng{fullhash}{FRFS1}
  \verb{doi}
  \verb 10.1007/JHEP12(2012)061
  \endverb
  \verb{eprint}
  \verb 1209.6215
  \endverb
  \field{pages}{061}
  \field{title}{{Merging meets matching in MC@NLO}}
  \field{volume}{1212}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{ATL-PHYS-PUB-2017-006}{report}{}
  \field{number}{ATL-PHYS-PUB-2017-006}
  \field{title}{{ATLAS simulation of boson plus jets processes in Run 2}}
  \verb{url}
  \verb https://cds.cern.ch/record/2261937
  \endverb
  \list{location}{1}{%
    {Geneva}%
  }
  \list{institution}{1}{%
    {CERN}%
  }
  \field{type}{techreport}
  \field{year}{2017}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{Cacciari:2008gp}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Cacciari}{C.}%
     {Matteo}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Salam}{S.}%
     {Gavin~P.}{G.~P.}%
     {}{}%
     {}{}}%
    {{}%
     {Soyez}{S.}%
     {Gregory}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CMSGPSG1}
  \strng{fullhash}{CMSGPSG1}
  \verb{doi}
  \verb 10.1088/1126-6708/2008/04/063
  \endverb
  \verb{eprint}
  \verb 0802.1189
  \endverb
  \field{pages}{063}
  \field{title}{{The Anti-k(t) jet clustering algorithm}}
  \field{volume}{04}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Siegert:2016bre}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Siegert}{S.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{SF1}
  \strng{fullhash}{SF1}
  \verb{doi}
  \verb 10.1088/1361-6471/aa5f29
  \endverb
  \verb{eprint}
  \verb 1611.07226
  \endverb
  \field{number}{4}
  \field{pages}{044007}
  \field{title}{{A practical guide to event generation for prompt photon
  production}}
  \field{volume}{G44}
  \field{journaltitle}{J. Phys.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2017}
\endentry

\entry{Frixione:1998jh}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Frixione}{F.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{FS1}
  \strng{fullhash}{FS1}
  \verb{doi}
  \verb 10.1016/S0370-2693(98)00454-7
  \endverb
  \verb{eprint}
  \verb hep-ph/9801442
  \endverb
  \field{pages}{369\bibrangedash 374}
  \field{title}{{Isolated photons in perturbative QCD}}
  \field{volume}{B429}
  \field{journaltitle}{Phys. Lett.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{1998}
\endentry

\entry{Frederix:2011ss}{article}{}
  \name{author}{6}{}{%
    {{}%
     {Frederix}{F.}%
     {Rikkert}{R.}%
     {}{}%
     {}{}}%
    {{}%
     {Frixione}{F.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Hirschi}{H.}%
     {Valentin}{V.}%
     {}{}%
     {}{}}%
    {{}%
     {Maltoni}{M.}%
     {Fabio}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Pittau}{P.}%
     {Roberto}{R.}%
     {}{}%
     {}{}}%
    {{}%
     {Torrielli}{T.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{FR+1}
  \strng{fullhash}{FRFSHVMFPRTP1}
  \verb{doi}
  \verb 10.1007/JHEP02(2012)099
  \endverb
  \verb{eprint}
  \verb 1110.4738
  \endverb
  \field{pages}{099}
  \field{title}{{Four-lepton production at hadron colliders: aMC@NLO
  predictions with theoretical uncertainties}}
  \field{volume}{02}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{Butterworth:2014efa}{article}{}
  \true{moreauthor}
  \name{author}{5}{}{%
    {{}%
     {Butterworth}{B.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Dissertori}{D.}%
     {G.}{G.}%
     {}{}%
     {}{}}%
    {{}%
     {Dittmaier}{D.}%
     {S.}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Florian}{F.}%
     {D.}{D.}%
     {de}{d.}%
     {}{}}%
    {{}%
     {Glover}{G.}%
     {N.}{N.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BJ+1}
  \strng{fullhash}{BJDGDSFDdGN+1}
  \verb{eprint}
  \verb 1405.1067
  \endverb
  \field{title}{{Les Houches 2013: Physics at TeV Colliders: Standard Model
  Working Group Report}}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2014}
\endentry

\entry{Cacciari:2006sm}{inproceedings}{}
  \name{author}{1}{}{%
    {{}%
     {Cacciari}{C.}%
     {Matteo}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CM1}
  \strng{fullhash}{CM1}
  \field{booktitle}{{Deep inelastic scattering. Proceedings, 14th International
  Workshop, DIS 2006, Tsukuba, Japan, April 20-24, 2006}}
  \verb{eprint}
  \verb hep-ph/0607071
  \endverb
  \field{note}{[,125(2006)]}
  \field{pages}{487\bibrangedash 490}
  \field{title}{{FastJet: A Code for fast $k_t$ clustering, and more}}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2006}
\endentry

\entry{Alioli:2010xd}{article}{}
  \name{author}{4}{}{%
    {{}%
     {Alioli}{A.}%
     {Simone}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Nason}{N.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Oleari}{O.}%
     {Carlo}{C.}%
     {}{}%
     {}{}}%
    {{}%
     {Re}{R.}%
     {Emanuele}{E.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{AS+1}
  \strng{fullhash}{ASNPOCRE1}
  \verb{doi}
  \verb 10.1007/JHEP06(2010)043
  \endverb
  \verb{eprint}
  \verb 1002.2581
  \endverb
  \field{pages}{043}
  \field{title}{{A general framework for implementing NLO calculations in
  shower Monte Carlo programs: the POWHEG BOX}}
  \field{volume}{1006}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2010}
\endentry

\entry{Hamilton:2012np}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Hamilton}{H.}%
     {Keith}{K.}%
     {}{}%
     {}{}}%
    {{}%
     {Nason}{N.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Zanderighi}{Z.}%
     {Giulia}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HKNPZG1}
  \strng{fullhash}{HKNPZG1}
  \verb{doi}
  \verb 10.1007/JHEP10(2012)155
  \endverb
  \verb{eprint}
  \verb 1206.3572
  \endverb
  \field{pages}{155}
  \field{title}{{MINLO: Multi-Scale Improved NLO}}
  \field{volume}{10}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2012}
\endentry

\entry{Hamilton:2012rf}{article}{}
  \name{author}{4}{}{%
    {{}%
     {Hamilton}{H.}%
     {Keith}{K.}%
     {}{}%
     {}{}}%
    {{}%
     {Nason}{N.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Oleari}{O.}%
     {Carlo}{C.}%
     {}{}%
     {}{}}%
    {{}%
     {Zanderighi}{Z.}%
     {Giulia}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HK+1}
  \strng{fullhash}{HKNPOCZG1}
  \verb{doi}
  \verb 10.1007/JHEP05(2013)082
  \endverb
  \verb{eprint}
  \verb 1212.4504
  \endverb
  \field{pages}{082}
  \field{title}{{Merging H/W/Z + 0 and 1 jet at NLO with no merging scale: a
  path to parton shower + NNLO matching}}
  \field{volume}{05}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2013}
\endentry

\entry{ATL-PHYS-PUB-2013-017}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{ATLAS Collaboration}}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \field{note}{ATL-PHYS-PUB-2013-017}
  \field{title}{{Example ATLAS tunes of Pythia8, Pythia6 and Powheg to an
  observable sensitive to $Z$ boson transverse momentum}}
  \verb{url}
  \verb https://cds.cern.ch/record/1629317
  \endverb
  \field{year}{2013}
\endentry

\entry{Golonka:2005pn}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Golonka}{G.}%
     {Piotr}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Was}{W.}%
     {Zbigniew}{Z.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{GPWZ1}
  \strng{fullhash}{GPWZ1}
  \verb{doi}
  \verb 10.1140/epjc/s2005-02396-4
  \endverb
  \verb{eprint}
  \verb hep-ph/0506026
  \endverb
  \field{pages}{97\bibrangedash 107}
  \field{title}{{PHOTOS Monte Carlo: A Precision tool for QED corrections in
  $Z$ and $W$ decays}}
  \field{volume}{C45}
  \field{journaltitle}{Eur. Phys. J.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2006}
\endentry

\entry{Davidson:2010ew}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Davidson}{D.}%
     {N.}{N.}%
     {}{}%
     {}{}}%
    {{}%
     {Przedzinski}{P.}%
     {T.}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Was}{W.}%
     {Z.}{Z.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DNPTWZ1}
  \strng{fullhash}{DNPTWZ1}
  \verb{doi}
  \verb 10.1016/j.cpc.2015.09.013
  \endverb
  \verb{eprint}
  \verb 1011.0937
  \endverb
  \field{pages}{86\bibrangedash 101}
  \field{title}{{PHOTOS interface in C++: Technical and Physics Documentation}}
  \field{volume}{199}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2016}
\endentry

\entry{Dulat:2015mca}{article}{}
  \name{author}{10}{}{%
    {{}%
     {Dulat}{D.}%
     {Sayipjamal}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Hou}{H.}%
     {Tie-Jiun}{T.-J.}%
     {}{}%
     {}{}}%
    {{}%
     {Gao}{G.}%
     {Jun}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Guzzi}{G.}%
     {Marco}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Huston}{H.}%
     {Joey}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Nadolsky}{N.}%
     {Pavel}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Pumplin}{P.}%
     {Jon}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Schmidt}{S.}%
     {Carl}{C.}%
     {}{}%
     {}{}}%
    {{}%
     {Stump}{S.}%
     {Daniel}{D.}%
     {}{}%
     {}{}}%
    {{}%
     {Yuan}{Y.}%
     {C.~P.}{C.~P.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DS+1}
  \strng{fullhash}{DSHTJGJGMHJNPPJSCSDYCP1}
  \verb{doi}
  \verb 10.1103/PhysRevD.93.033006
  \endverb
  \verb{eprint}
  \verb 1506.07443
  \endverb
  \field{number}{3}
  \field{pages}{033006}
  \field{title}{{New parton distribution functions from a global analysis of
  quantum chromodynamics}}
  \field{volume}{93}
  \field{journaltitle}{Phys. Rev. D}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2016}
\endentry

\entry{Pumplin:2002vw}{article}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Pumplin}{P.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{PJ+1}
  \strng{fullhash}{PJ+1}
  \verb{doi}
  \verb 10.1088/1126-6708/2002/07/012
  \endverb
  \verb{eprint}
  \verb hep-ph/0201195
  \endverb
  \field{pages}{012}
  \field{title}{{New Generation of Parton Distributions with Uncertainties From
  Global QCD Analysis}}
  \field{volume}{0207}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{year}{2002}
\endentry

\entry{Mangano:2002ea}{article}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Mangano}{M.}%
     {Michelangelo~L.}{M.~L.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{MML+1}
  \strng{fullhash}{MML+1}
  \verb{doi}
  \verb 10.1088/1126-6708/2003/07/001
  \endverb
  \verb{eprint}
  \verb hep-ph/0206293
  \endverb
  \field{pages}{001}
  \field{title}{{ALPGEN, a generator for hard multiparton processes in hadronic
  collisions}}
  \field{volume}{07}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{year}{2003}
\endentry

\entry{Sjostrand:2006za}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Sj{\"o}strand}{S.}%
     {Torbjorn}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Mrenna}{M.}%
     {Stephen}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Skands}{S.}%
     {Peter~Z.}{P.~Z.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{STMSSPZ1}
  \strng{fullhash}{STMSSPZ1}
  \verb{doi}
  \verb 10.1088/1126-6708/2006/05/026
  \endverb
  \verb{eprint}
  \verb hep-ph/0603175
  \endverb
  \field{pages}{026}
  \field{title}{{PYTHIA 6.4 Physics and Manual}}
  \field{volume}{0605}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{year}{2006}
\endentry

\entry{Skands:2010ak}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Skands}{S.}%
     {Peter~Zeiler}{P.~Z.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{SPZ1}
  \strng{fullhash}{SPZ1}
  \verb{doi}
  \verb 10.1103/PhysRevD.82.074018
  \endverb
  \verb{eprint}
  \verb 1005.3457
  \endverb
  \field{pages}{074018}
  \field{title}{{Tuning Monte Carlo Generators: The Perugia Tunes}}
  \field{volume}{82}
  \field{journaltitle}{Phys. Rev. D}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2010}
\endentry

\entry{Alwall:2007fs}{article}{}
  \true{moreauthor}
  \name{author}{5}{}{%
    {{}%
     {Alwall}{A.}%
     {Johan}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Hoche}{H.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Krauss}{K.}%
     {F.}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Lavesson}{L.}%
     {N.}{N.}%
     {}{}%
     {}{}}%
    {{}%
     {Lonnblad}{L.}%
     {L.}{L.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{AJ+2}
  \strng{fullhash}{AJHSKFLNLL+1}
  \verb{doi}
  \verb 10.1140/epjc/s10052-007-0490-5
  \endverb
  \verb{eprint}
  \verb 0706.2569
  \endverb
  \field{pages}{473\bibrangedash 500}
  \field{title}{{Comparative study of various algorithms for the merging of
  parton showers and matrix elements in hadronic collisions}}
  \field{volume}{53}
  \field{journaltitle}{Eur. Phys. J. C}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Lai:2010vv}{article}{}
  \true{moreauthor}
  \name{author}{5}{}{%
    {{}%
     {Lai}{L.}%
     {Hung-Liang}{H.-L.}%
     {}{}%
     {}{}}%
    {{}%
     {Guzzi}{G.}%
     {Marco}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Huston}{H.}%
     {Joey}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Li}{L.}%
     {Zhao}{Z.}%
     {}{}%
     {}{}}%
    {{}%
     {Nadolsky}{N.}%
     {Pavel~M.}{P.~M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{LHL+1}
  \strng{fullhash}{LHLGMHJLZNPM+1}
  \verb{doi}
  \verb 10.1103/PhysRevD.82.074024
  \endverb
  \verb{eprint}
  \verb 1007.2241
  \endverb
  \field{pages}{074024}
  \field{title}{{New parton distributions for collider physics}}
  \field{volume}{82}
  \field{journaltitle}{Phys. Rev. D}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2010}
\endentry

\entry{Ball:2014uwa}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{NNPDF Collaboration, R.D. Ball et al.}}{N.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{N1}
  \strng{fullhash}{N1}
  \verb{doi}
  \verb 10.1007/JHEP04(2015)040
  \endverb
  \verb{eprint}
  \verb 1410.8849
  \endverb
  \field{pages}{040}
  \field{title}{{Parton distributions for the LHC Run II}}
  \field{volume}{1504}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2015}
\endentry

\entry{Harland-Lang:2014zoa}{article}{}
  \name{author}{4}{}{%
    {{}%
     {Harland-Lang}{H.-L.}%
     {L.A.}{L.}%
     {}{}%
     {}{}}%
    {{}%
     {Martin}{M.}%
     {A.D.}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Motylinski}{M.}%
     {P.}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Thorne}{T.}%
     {R.S.}{R.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HLL+1}
  \strng{fullhash}{HLLMAMPTR1}
  \verb{doi}
  \verb 10.1140/epjc/s10052-015-3397-6
  \endverb
  \verb{eprint}
  \verb 1412.3989
  \endverb
  \field{number}{5}
  \field{pages}{204}
  \field{title}{{Parton distributions in the LHC era: MMHT 2014 PDFs}}
  \field{volume}{75}
  \field{journaltitle}{Eur. Phys. J. C}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2015}
\endentry

\entry{Nason:2004rx}{article}{}
  \name{author}{1}{}{%
    {{}%
     {Nason}{N.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{NP1}
  \strng{fullhash}{NP1}
  \verb{doi}
  \verb 10.1088/1126-6708/2004/11/040
  \endverb
  \verb{eprint}
  \verb hep-ph/0409146
  \endverb
  \field{pages}{040}
  \field{title}{{A New method for combining NLO QCD with shower Monte Carlo
  algorithms}}
  \field{volume}{0411}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{year}{2004}
\endentry

\entry{Frixione:2007vw}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Frixione}{F.}%
     {Stefano}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Nason}{N.}%
     {Paolo}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Oleari}{O.}%
     {Carlo}{C.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{FSNPOC1}
  \strng{fullhash}{FSNPOC1}
  \verb{doi}
  \verb 10.1088/1126-6708/2007/11/070
  \endverb
  \verb{eprint}
  \verb 0709.2092
  \endverb
  \field{pages}{070}
  \field{title}{{Matching NLO QCD computations with Parton Shower simulations:
  the POWHEG method}}
  \field{volume}{11}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2007}
\endentry

\entry{Boos:2001cv}{inproceedings}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Boos}{B.}%
     {E.}{E.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BE+1}
  \strng{fullhash}{BE+1}
  \field{booktitle}{{Physics at TeV colliders. Proceedings, Euro Summer School,
  Les Houches, France, May 21-June 1, 2001}}
  \verb{eprint}
  \verb hep-ph/0109068
  \endverb
  \field{title}{{Generic user process interface for event generators}}
  \verb{url}
  \verb http://lss.fnal.gov/archive/preprint/fermilab-conf-01-496-t.shtml
  \endverb
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2001}
\endentry

\entry{Alwall:2006yp}{article}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Alwall}{A.}%
     {Johan}{J.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{AJ+2}
  \strng{fullhash}{AJ+1}
  \field{booktitle}{{Monte Carlos for the LHC: A Workshop on the Tools for LHC
  Event Simulation (MC4LHC) Geneva, Switzerland, July 17-16, 2006}}
  \verb{doi}
  \verb 10.1016/j.cpc.2006.11.010
  \endverb
  \verb{eprint}
  \verb hep-ph/0609017
  \endverb
  \field{pages}{300\bibrangedash 304}
  \field{title}{{A Standard format for Les Houches event files}}
  \field{volume}{176}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2007}
\endentry

\entry{Sjostrand:2014zea}{article}{}
  \name{author}{10}{}{%
    {{}%
     {Sj{\"o}strand}{S.}%
     {Torbj{\"o}rn}{T.}%
     {}{}%
     {}{}}%
    {{}%
     {Ask}{A.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Christiansen}{C.}%
     {Jesper~R.}{J.~R.}%
     {}{}%
     {}{}}%
    {{}%
     {Corke}{C.}%
     {Richard}{R.}%
     {}{}%
     {}{}}%
    {{}%
     {Desai}{D.}%
     {Nishita}{N.}%
     {}{}%
     {}{}}%
    {{}%
     {Ilten}{I.}%
     {Philip}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Mrenna}{M.}%
     {Stephen}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Prestel}{P.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Rasmussen}{R.}%
     {Christine~O.}{C.~O.}%
     {}{}%
     {}{}}%
    {{}%
     {Skands}{S.}%
     {Peter~Z.}{P.~Z.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{ST+1}
  \strng{fullhash}{STASCJRCRDNIPMSPSRCOSPZ1}
  \verb{doi}
  \verb 10.1016/j.cpc.2015.01.024
  \endverb
  \verb{eprint}
  \verb 1410.3012
  \endverb
  \field{pages}{159\bibrangedash 177}
  \field{title}{{An Introduction to PYTHIA 8.2}}
  \field{volume}{191}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2015}
\endentry

\entry{Nakamura:2010zzi}{article}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Nakamura}{N.}%
     {K.}{K.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{NK+1}
  \strng{fullhash}{NK+1}
  \verb{doi}
  \verb 10.1088/0954-3899/37/7A/075021
  \endverb
  \field{pages}{075021}
  \field{title}{{Review of particle physics}}
  \field{volume}{G37}
  \field{journaltitle}{J. Phys.}
  \field{year}{2010}
\endentry

\entry{Groom:2000in}{article}{}
  \true{moreauthor}
  \name{author}{1}{}{%
    {{}%
     {Groom}{G.}%
     {Donald~E.}{D.~E.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{GDE+1}
  \strng{fullhash}{GDE+1}
  \field{pages}{1\bibrangedash 878}
  \field{title}{{Review of particle physics. Particle Data Group}}
  \field{volume}{C15}
  \field{journaltitle}{Eur. Phys. J.}
  \field{year}{2000}
\endentry

\entry{Frixione:2007nw}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{S. Frixione et al.}}{S.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{S1}
  \strng{fullhash}{S1}
  \verb{doi}
  \verb 10.1088/1126-6708/2007/09/126
  \endverb
  \verb{eprint}
  \verb 0707.3088
  \endverb
  \field{pages}{126}
  \field{title}{{A positive-weight next-to-leading-order Monte Carlo for heavy
  flavour hadroproduction}}
  \field{volume}{0709}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2007}
\endentry

\entry{Hoeche:2009rj}{article}{}
  \name{author}{4}{}{%
    {{}%
     {Hoeche}{H.}%
     {Stefan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Krauss}{K.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
    {{}%
     {Schumann}{S.}%
     {Steffen}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Siegert}{S.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{HS+2}
  \strng{fullhash}{HSKFSSSF1}
  \verb{doi}
  \verb 10.1088/1126-6708/2009/05/053
  \endverb
  \verb{eprint}
  \verb 0903.1219
  \endverb
  \field{pages}{053}
  \field{title}{{QCD matrix elements and truncated showers}}
  \field{volume}{0905}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2009}
\endentry

\entry{Buckley:2010ar}{article}{}
  \name{author}{8}{}{%
    {{}%
     {Buckley}{B.}%
     {Andy}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Butterworth}{B.}%
     {Jonathan}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Lonnblad}{L.}%
     {Leif}{L.}%
     {}{}%
     {}{}}%
    {{}%
     {Grellscheid}{G.}%
     {David}{D.}%
     {}{}%
     {}{}}%
    {{}%
     {Hoeth}{H.}%
     {Hendrik}{H.}%
     {}{}%
     {}{}}%
    {{}%
     {Monk}{M.}%
     {James}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Schulz}{S.}%
     {Holger}{H.}%
     {}{}%
     {}{}}%
    {{}%
     {Siegert}{S.}%
     {Frank}{F.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BA+1}
  \strng{fullhash}{BABJLLGDHHMJSHSF1}
  \verb{doi}
  \verb 10.1016/j.cpc.2013.05.021
  \endverb
  \verb{eprint}
  \verb 1003.0694
  \endverb
  \field{pages}{2803\bibrangedash 2819}
  \field{title}{{Rivet user manual}}
  \field{volume}{184}
  \field{journaltitle}{Comput. Phys. Commun.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2013}
\endentry

\entry{Aaboud:2016xii}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{ATLAS Collaboration}}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \verb{doi}
  \verb 10.1140/epjc/s10052-017-4766-0
  \endverb
  \verb{eprint}
  \verb 1610.09978
  \endverb
  \field{number}{4}
  \field{pages}{220}
  \field{title}{{Measurement of jet activity produced in top-quark events with
  an electron, a muon and two $b$-tagged jets in the final state in $pp$
  collisions at $\sqrt{s}=13$ TeV with the ATLAS detector}}
  \field{volume}{C77}
  \field{journaltitle}{Eur. Phys. J.}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ex}
  \field{year}{2017}
\endentry

\entry{Cacciari:2007fd}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Cacciari}{C.}%
     {Matteo}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Salam}{S.}%
     {Gavin~P.}{G.~P.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CMSGP1}
  \strng{fullhash}{CMSGP1}
  \verb{doi}
  \verb 10.1016/j.physletb.2007.09.077
  \endverb
  \verb{eprint}
  \verb 0707.1378
  \endverb
  \field{pages}{119\bibrangedash 126}
  \field{title}{{Pileup subtraction using jet areas}}
  \field{volume}{659}
  \field{journaltitle}{Phys. Lett. B}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{Cacciari:2008gn}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Cacciari}{C.}%
     {Matteo}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Salam}{S.}%
     {Gavin~P.}{G.~P.}%
     {}{}%
     {}{}}%
    {{}%
     {Soyez}{S.}%
     {Gregory}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CMSGPSG1}
  \strng{fullhash}{CMSGPSG1}
  \verb{doi}
  \verb 10.1088/1126-6708/2008/04/005
  \endverb
  \verb{eprint}
  \verb 0802.1188
  \endverb
  \field{pages}{005}
  \field{title}{{The Catchment Area of Jets}}
  \field{volume}{0804}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{PERF-2012-02}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{ATLAS Collaboration}}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \verb{doi}
  \verb 10.1007/JHEP09(2013)076
  \endverb
  \verb{eprint}
  \verb 1306.4945
  \endverb
  \field{pages}{076}
  \field{title}{{Performance of jet substructure techniques for large-$R$ jets
  in proton--proton collisions at $\sqrt{s} = 7\;\mbox{TeV}$ using the ATLAS
  detector}}
  \field{volume}{09}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ex}
  \field{year}{2013}
\endentry

\entry{antikt}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Cacciari}{C.}%
     {Matteo}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Salam}{S.}%
     {Gavin~P.}{G.~P.}%
     {}{}%
     {}{}}%
    {{}%
     {Soyez}{S.}%
     {Gregory}{G.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CMSGPSG1}
  \strng{fullhash}{CMSGPSG1}
  \verb{doi}
  \verb 10.1088/1126-6708/2008/04/063
  \endverb
  \verb{eprint}
  \verb 0802.1189
  \endverb
  \field{pages}{063}
  \field{title}{{The anti-$k_{t}$ jet clustering algorithm}}
  \field{volume}{0804}
  \field{journaltitle}{JHEP}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ph}
  \field{year}{2008}
\endentry

\entry{TOPQ-2015-06}{article}{}
  \name{author}{1}{}{%
    {{}%
     {{ATLAS Collaboration}}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \verb{doi}
  \verb 10.1140/epjc/s10052-016-4366-4
  \endverb
  \verb{eprint}
  \verb 1511.04716
  \endverb
  \field{pages}{538}
  \field{title}{{Measurements of top-quark pair differential cross-sections in
  the lepton+jets channel in $pp$ collisions at $\sqrt{s} = 8\;\mbox{TeV}$
  using the ATLAS detector}}
  \field{volume}{76}
  \field{journaltitle}{Eur. Phys. J. C}
  \field{eprinttype}{arXiv}
  \field{eprintclass}{hep-ex}
  \field{year}{2016}
\endentry

\lossort
\endlossort

\endinput
