\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Common generator details}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}\textsc {Sherpa}\xspace MEPS@NLO}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}\textsc {MadGraph5\_aMC@NLO}\xspace +\textsc {Pythia8}\xspace with CKKW-L merging}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}\textsc {MadGraph5\_aMC@NLO}\xspace {}+{}\textsc {Pythia8}\xspace using \textsc {FxFx}\xspace merging}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}V+jets}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}\textsc {Sherpa}\xspace MEPS@NLO}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}\textsc {MadGraph5}\_a\textsc {MC@NLO} using CKKW-L}{5}{subsection.2.2}
\contentsline {paragraph}{\nonumberline H}{5}{section*.2}
\contentsline {paragraph}{\nonumberline N}{5}{section*.3}
\contentsline {subsection}{\numberline {2.3}\textsc {MadGraph5\_aMC@NLO}\xspace using \textsc {FxFx}\xspace }{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}\textsc {Powheg MiNLO}\xspace }{6}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}\textsc {Alpgen}\xspace }{6}{subsection.2.5}
\contentsline {section}{\numberline {3}Multiboson}{8}{section.3}
\contentsline {paragraph}{\nonumberline \textsc {Sherpa}\xspace \texttt {v2.1.1}}{8}{section*.5}
\contentsline {paragraph}{\nonumberline \textsc {Sherpa}\xspace \texttt {v2.2.1}}{8}{section*.6}
\contentsline {paragraph}{\nonumberline \textsc {Sherpa}\xspace \texttt {v2.2.2}}{8}{section*.7}
\contentsline {subsection}{\numberline {3.1}\texttt {PowhegBox}\xspace }{9}{subsection.3.1}
\contentsline {section}{\numberline {4}\ensuremath {t\bar {t}}\xspace }{10}{section.4}
\contentsline {subsection}{\numberline {4.1}MC Generator settings}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Measurements of \ensuremath {t\bar {t}}\xspace production}{11}{subsection.4.2}
\contentsline {part}{Appendix}{14}{part*.9}
